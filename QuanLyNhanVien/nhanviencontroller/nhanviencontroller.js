function layThongTinTuForm() {
  var taiKhoanNV = document.getElementById("tknv").value.trim();
  var tenNV = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value.trim();
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value.trim() * 1;

  var nv = new NhanVien(taiKhoanNV,tenNV,email,matKhau,ngayLam,luongCB,chucVu,gioLam);
  return nv;
}

function renderDanhSachNV(list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var content = `
        <tr>
        <td>${item.tk}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.ngayLam}</td>
        <td>${item.luongCB}</td>
        <td>${item.chucVu}</td>
        <td>${item.gioLam}</td>
        <td>${item.handleTongLuong()}</td>
        <td>${item.handleXepLoai()}</td>
        <td>
        <button class="btn btn-danger" onclick= "xoaNV('${item.tk}')">Xoá</button> 
        <button data-toggle="modal" data-target="#myModal" class="btn btn-warning" onclick= "suaNV('${item.tk}')">Sửa</button>
        </td>
        </tr>`;
    contentHTML += content;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

//Tìm kiếm vị trí
function timKiemViTri(id, arr) {
  // for (var index = 0; index < arr.length; index++) {
  //   var nv = arr[index];
  //   if (nv.tk == id) {
  //     return index;
  //   }
  // }
  // return -1;
  return arr.findIndex(function(sv){
    return sv.tk == id;
  })
}

//show thông tin nhân viên lên form
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.tk;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
