//Tạo array
var dsnv = [];
const DSNV = "DSNV";

//Lấy danh sach sinh viên từ localstorage dạng json
var dsnvLocalStorage = localStorage.getItem(DSNV);

// convert json thành array rồi gán lại cho danh sách nhân viên
if (JSON.parse(dsnvLocalStorage)) {
  data = JSON.parse(dsnvLocalStorage);
  for (var index = 0; index < data.length; index++)
    var current = data[index];
  var nv = new NhanVien(
    current.tk,
    current.ten,
    current.email,
    current.matKhau,
    current.ngayLam,
    current.luongCB,
    current.chucVu,
    current.gioLam
  );
  dsnv.push(nv);
  renderDanhSachNV(dsnv);
}

// (1) Lưu danh sách
function saveLocalStorage() {
  //convert array thành json
  var dsnvJson = JSON.stringify(dsnv);

  //Lưu xuống LocalStorage
  localStorage.setItem(DSNV, dsnvJson);
}
//HANDLE THÊM NHÂN VIÊN
function themUser() {
  var newNV = layThongTinTuForm();



  // validate tài khoản
  var isValid =
  validator.kiemTraRong(newNV.tk,"tbTKNV","Tài khoản không được để trống")&&
  validator.kiemTraTkNhanVien(newNV.tk,dsnv)&&
  validator.kiemTraDoDai(newNV.tk,"tbTKNV",4,6)&&
  validator.kiemTraChuoiSo(newNV.tk,"tbTKNV");

  //validate tên
  isValid = isValid & validator.kiemTraRong(newNV.ten,"tbTen","Họ và tên không được để trống")&&
  validator.kiemTraChuoiChu(newNV.ten,'tbTen');
 
  //validate email
  isValid = isValid & validator.kiemTraRong(newNV.email,"tbEmail","Email không được để trống")&&
  validator.kiemTraEmail(newNV.email,'tbEmail');
  
  //validate passwword
  isValid = isValid & validator.kiemTraRong(newNV.matKhau,"tbMatKhau","Mật khẩu không được để trống")&&
  validator.kiemTraDoDai(newNV.matKhau,"tbMatKhau",6,10)&&
  validator.kiemTraMatKhau(newNV.matKhau,'tbMatKhau') ;

  //validate ngày làm
  isValid = isValid & validator.kiemTraRong(newNV.ngayLam,"tbNgay","Ngày làm không được để trống")&&
  validator.kiemTraNgayLam(newNV.ngayLam,'tbNgay');

  //validate lương
  isValid = isValid & validator.kiemTraRong(newNV.luongCB,"tbLuongCB","Lương không được để trống")&&
  validator.kiemTraChuoiSo(newNV.luongCB,'tbLuongCB')&&
  validator.kiemTraMinMax(newNV.luongCB,'tbLuongCB',1000000, 20000000);

  //validate chức vụ
  isValid = isValid & validator.kiemTraChucVu(newNV.chucVu,'tbChucVu');

  // validate giờ làm 
  isValid = isValid & validator.kiemTraRong(newNV.gioLam,"tbGiolam","Giờ làm không được để trống")&&
  validator.kiemTraMinMax(newNV.gioLam,'tbGiolam',80,200);


  if (isValid == false) {
    return;
  }
  dsnv.push(newNV);
  //push dữ liệu

  saveLocalStorage();

  //Xuất dữ liệu nhập vào
  renderDanhSachNV(dsnv);
}

//HANDLE XOÁ NHÂN VIÊN
function xoaNV(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    dsnv.splice(index, 1);
    saveLocalStorage();
    renderDanhSachNV(dsnv);
  }
}

//HANDLE SỬA NHÂN VIÊN
function suaNV(id) {
  var index = timKiemViTri(id, dsnv);
  if (index !== -1) {
    var nv = dsnv[index];
    showThongTinLenForm(nv);
  }
}

//HANDLE CẬP NHẬT NHÂN VIÊN
function capNhatNV() {
  var nvEdited = layThongTinTuForm();
  var index = timKiemViTri(nvEdited.tk, dsnv);
  if (index !== -1) {
    dsnv[index] = nvEdited;

    // validate tài khoản
  var isValid =
  validator.kiemTraRong(nvEdited.tk,"tbTKNV","Tài khoản không được để trống")&&
  validator.kiemTraTkNhanVien(nvEdited.tk,dsnv)&&
  validator.kiemTraDoDai(nvEdited.tk,"tbTKNV",4,6)&&
  validator.kiemTraChuoiSo(nvEdited.tk,"tbTKNV");

  //validate tên
  isValid = isValid & validator.kiemTraRong(nvEdited.ten,"tbTen","Họ và tên không được để trống")&&
  validator.kiemTraChuoiChu(nvEdited.ten,'tbTen');
 
  //validate email
  isValid = isValid & validator.kiemTraRong(nvEdited.email,"tbEmail","Email không được để trống")&&
  validator.kiemTraEmail(nvEdited.email,'tbEmail');
  
  //validate passwword
  isValid = isValid & validator.kiemTraRong(nvEdited.matKhau,"tbMatKhau","Mật khẩu không được để trống")&&
  validator.kiemTraDoDai(nvEdited.matKhau,"tbMatKhau",6,10)&&
  validator.kiemTraMatKhau(nvEdited.matKhau,'tbMatKhau') ;

  //validate ngày làm
  isValid = isValid & validator.kiemTraRong(nvEdited.ngayLam,"tbNgay","Ngày làm không được để trống")&&
  validator.kiemTraNgayLam(nvEdited.ngayLam,'tbNgay');

  //validate lương
  isValid = isValid & validator.kiemTraRong(nvEdited.luongCB,"tbLuongCB","Lương không được để trống")&&
  validator.kiemTraChuoiSo(nvEdited.luongCB,'tbLuongCB')&&
  validator.kiemTraMinMax(nvEdited.luongCB,'tbLuongCB',1000000, 20000000);

  //validate chức vụ
  isValid = isValid & validator.kiemTraChucVu(nvEdited.chucVu,'tbChucVu');

  // validate giờ làm 
  isValid = isValid & validator.kiemTraRong(nvEdited.gioLam,"tbGiolam","Giờ làm không được để trống")&&
  validator.kiemTraMinMax(nvEdited.gioLam,'tbGiolam',80,200);

  if (isValid == false) {
    return;
  }


    saveLocalStorage();
    renderDanhSachNV(dsnv);
  }
}

//HANDLE RESET NHÂN VIÊN
function resetForm(){
  document.getElementById("formQLNV").reset();
}

//HANDLE TÌM KIẾM NHÂN VIÊN THEO LOẠI
function timNV(){
  console.log("YES");
  var searchName = document.getElementById("searchName").value;
  var searchList = [];
 dsnv.forEach((nv) => {
  if(searchName == nv.handleXepLoai()){
    searchList.push(nv);
    renderDanhSachNV(searchList)
  }
  if(searchName ==""){
    renderDanhSachNV(dsnv)
  }
 })
 

}









