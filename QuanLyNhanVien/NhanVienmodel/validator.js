var validator = {
  kiemTraRong: function (valueInput, idError, message) {
    if (valueInput == "") {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    }
  },

  kiemTraTkNhanVien: function (tknv, dsnv) {
    var index = dsnv.findIndex((nv) => {
      return nv.tk == tknv;
    });
    if (index !== -1) {
      document.getElementById("tbTKNV").innerText =
        "Tài khoản nhân viên đã tồn tại";
      document.getElementById("tbTKNV").style.display = "block";
      return false;
    } else {
      document.getElementById("tbTKNV").innerText = "";
      document.getElementById("tbTKNV").style.display = "none";
      return true;
    }
  },
  kiemTraDoDai: function (valueInput, idError, min, max) {
    var inputLength = valueInput.length;
    if (inputLength < min || inputLength > max) {
      document.getElementById(
        idError
      ).innerText = `Độ dài phải từ ${min} đến ${max} ký tự`;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    }
  },
  kiemTraChuoiSo: function (valueInput, idError) {
    var regex = /^[0-9]+$/;

    if (regex.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Trường này chỉ được nhập số";
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
  kiemTraChuoiChu: function (valueInput, idError) {
    regex =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
      if (regex.test(valueInput)) {
        document.getElementById(idError).innerText = "";
        document.getElementById(idError).style.display = 'none';
        return true;
      } else {
        document.getElementById(idError).innerText =
          "Trường này chỉ được nhập chữ";
        document.getElementById(idError).style.display = 'block';
        return false;
      }
  },
  kiemTraEmail: function (valueInput, idError) {
    regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = 'none';
      return true;
    } else {
      document.getElementById(idError).innerText = "Email phải đúng định dạng";
      document.getElementById(idError).style.display = 'block';
      return false;
    }
  },
  kiemTraMatKhau: function (valueInput, idError) {
    regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;

    if (regex.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = 'none';
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Mật khẩu phải có số, ký tự in hoa và ký tự đặc biệt";
        document.getElementById(idError).style.display = 'block';
      return false;
    }
  },
  kiemTraMinMax: function (valueInput, idError, min, max) {
    if (valueInput * 1 < min || valueInput * 1 > max) {
      document.getElementById(idError).innerText = `Giá trị phải từ ${min} - ${max}`;
      document.getElementById(idError).style.display = 'block';
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = 'none';
      return true;
    }
  },
  kiemTraNgayLam: function (valueInput, idError) {
    regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

    if (regex.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = 'none';
      return true;
    } else {
      document.getElementById(idError).innerText = "Ngày phải theo định dạng mm/dd/yyyy";
      document.getElementById(idError).style.display = 'block';
      return false;
    }
  },
  kiemTraChucVu: function (valueInput, idError) {
    if (valueInput == "Sếp" || valueInput == "Trưởng phòng" || valueInput == "Nhân viên") {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = 'none';
      return true;
    } else {
      document.getElementById(idError).innerText = "Chưa chọn chức vụ";
      document.getElementById(idError).style.display = 'block';
      return false;
    }
  },




  

};
