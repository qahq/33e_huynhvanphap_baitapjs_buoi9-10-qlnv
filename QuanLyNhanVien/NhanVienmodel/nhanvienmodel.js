        function NhanVien(taiKhoanNV,tenNV,email,matKhau,ngayLam,luongCB,chucVu,gioLam){
            this.tk=taiKhoanNV;
            this.ten=tenNV;
            this.email=email;
            this.matKhau=matKhau;
            this.ngayLam=ngayLam;
            this.luongCB=luongCB;
            this.chucVu=chucVu;
            this.gioLam=gioLam;
            this.handleTongLuong=function(){
                var tongLuong = 0;
                if (chucVu == "Sếp") {
                   tongLuong = luongCB*3;
                   } else if (chucVu == "Trưởng phòng") {
                    tongLuong = luongCB*2;
                   } else {
                    tongLuong = luongCB;
                   }
                   return tongLuong
            }
            this.handleXepLoai=function(){
              var xepLoai ="";
              if (this.gioLam >= 192) {
                  xepLoai = "Xuất sắc";
                } else if (this.gioLam >= 176 && this.gioLam < 192) {
                  xepLoai = "Giỏi";
                } else if (this.gioLam >= 160 && this.gioLam < 176) {
                  xepLoai = "Khá";
                } else {
                  xepLoai = "Trung bình";
                }
                return xepLoai;
          }
        }